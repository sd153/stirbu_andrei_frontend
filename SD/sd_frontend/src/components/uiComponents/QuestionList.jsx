import React from "react";
import { Label, Card, Header } from "semantic-ui-react";
import { Link } from "react-router-dom";

const QuestionList = (props) => {
	return (
		<Card.Group
			centered
			style={{ maxHeight: 700, overflow: "hidden", overflowY: "scroll" }}
		>
			{props.questions.length === 0 ? (
				<Header
					as="h2"
					style={{
						margin: "0",
						position: "absolute",
						top: "30%",
						left: "46%",
					}}
				>
					No results found
				</Header>
			) : (
				props.questions.map((question) => (
					<Card style={{ width: 1000 }} key={question.id}>
						<Card.Content>
							<Label as="a" color="red" ribbon>
								Votes: {question.questionVotes}
							</Label>
							<span
								style={{
									display: "table",
									margin: "0 auto",
									fontSize: 20,
								}}
							>
								<Link
									to={{
										pathname: `/question/${question.id}`,
										state: { question: question },
									}}
								>
									{question.title}
								</Link>
							</span>
							<span
								style={{
									float: "right",
									marginRight: 30,
									fontSize: 15,
								}}
							>
								{question.questionDate}
							</span>
							<br />
							<br />
							<Label as="a" color="blue" ribbon>
								Answers: {question.answerCount}
							</Label>
							<Card.Meta textAlign="center">
								<span>Author: {question.userName}</span>
							</Card.Meta>
							<Card.Description style={{ marginLeft: 100 }}>
								{question.questionText}
							</Card.Description>
						</Card.Content>
						<Card.Content extra>
							{question.tagList.map((tag) => (
								<Label as="a" key={tag.id}>
									{tag.tagName}
								</Label>
							))}
						</Card.Content>
					</Card>
				))
			)}
		</Card.Group>
	);
};

export default QuestionList;
