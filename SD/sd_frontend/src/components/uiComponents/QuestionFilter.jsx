import React, { useState, useEffect } from "react";
import { Dropdown } from "semantic-ui-react";

const optionsType = [
	{ key: "question", text: "by question", value: "question" },
	{ key: "tag", text: "by tag", value: "tag" },
];

const QuestionFilter = ({ initialQuestions, changeQuestions, tags }) => {
	const [searchType, setSearchType] = useState("question");
	const [options, setOptions] = useState([]);

	const setInitialOptions = () => {
		setOptions(
			tags.map((tag) => {
				return { key: tag.id, text: tag.tagName, value: tag };
			})
		);
	};

	useEffect(() => {
		setInitialOptions();
		// eslint-disable-next-line
	}, [tags]);

	const onSearchTypeChange = (event, data) => {
		setSearchType(data.value);
		changeQuestions(initialQuestions);

		if (data.value === "question") {
			setOptions([]);
		} else {
			setInitialOptions();
		}
	};

	const onFilterChange = (event, data) => {
		if (searchType === "question") {
			const value = event.target.value.toLowerCase();

			if (value.trim().length === 0) {
				changeQuestions(initialQuestions);
				return;
			}

			changeQuestions(
				initialQuestions.filter((question) =>
					question.title.toLowerCase().includes(value)
				)
			);
		} else {
			if (
				data.value === null ||
				data.value === undefined ||
				data.value.length === 0
			) {
				changeQuestions(initialQuestions);
				return;
			}

			changeQuestions(
				initialQuestions.filter((question) => {
					let containsTag = false;
					question.tagList.forEach((tag) => {
						if (
							data.value
								.map((t) => t.tagName)
								.includes(tag.tagName)
						) {
							containsTag = true;
							return;
						}
					});

					return containsTag;
				})
			);
		}
	};

	return (
		<>
			<Dropdown
				placeholder="Search..."
				icon="search"
				className="icon"
				fluid
				multiple
				search
				selection
				options={searchType === "tag" ? options : []}
				style={{ width: 500 }}
				labeled
				onSearchChange={onFilterChange}
				onChange={onFilterChange}
			/>
			<Dropdown
				defaultValue="question"
				options={optionsType}
				onChange={onSearchTypeChange}
				button
			/>
		</>
	);
};

export default QuestionFilter;
