import React, { useState, useEffect } from "react";
import { Modal, Form, Button } from "semantic-ui-react";

const emptyModel = {
	title: "",
	questionText: "",
	questionDate: new Date(),
	questionVotes: 0,
	userID: 0,
	tagList: [],
};

const QuestionFormPanel = ({ onClose, open, onSave, userID, tags }) => {
	const [question, setQuestion] = useState({ ...emptyModel, userID: userID });
	const [options, setOptions] = useState([]);

	useEffect(() => {
		setQuestion((prevQuestion) => ({ ...prevQuestion, userID: userID }));
	}, [userID]);

	useEffect(() => {
		setOptions(
			tags.map((tag) => {
				return { key: tag.id, text: tag.tagName, value: tag };
			})
		);
	}, [tags]);

	const handleTextChange = (property, value) => {
		setQuestion((prevQuestion) => {
			return { ...prevQuestion, [property]: value };
		});
	};

	const onSelectChange = (event, data) => {
		setQuestion((prevQuestion) => {
			return { ...prevQuestion, tagList: data.value };
		});
	};

	const onSaveQuestion = async () => {
		await onSave(question);
		onClose();
		setQuestion(emptyModel);
	};

	const onCloseModal = () => {
		setQuestion(emptyModel);
		onClose();
	};

	const isSaveDisabled = () => {
		return (
			question === null ||
			question.title === null ||
			question.questionText === null ||
			question.title.trim().length === 0 ||
			question.questionText.trim().length === 0
		);
	};

	return (
		<Modal onClose={onCloseModal} open={open}>
			<Modal.Header>Ask a question</Modal.Header>
			<Modal.Content>
				<Form>
					<Form.Group widths="equal">
						<Form.Input
							label="Title"
							placeholder="Be specific and imagine you're asking a question to another person"
							onChange={(event) =>
								handleTextChange("title", event.target.value)
							}
							value={question.title}
						/>
					</Form.Group>
					<Form.TextArea
						label="Body"
						placeholder="Include all the information someone would need to answer your question"
						style={{ minHeight: 250 }}
						onChange={(event) =>
							handleTextChange("questionText", event.target.value)
						}
						value={question.questionText}
					/>
					<Form.Select
						fluid
						label="Tags"
						options={options}
						multiple
						search
						placeholder="Add tags to describe what your question is about"
						onChange={onSelectChange}
					/>
				</Form>
			</Modal.Content>
			<Modal.Actions>
				<Button
					color="black"
					onClick={onCloseModal}
					content="Close"
					negative
				/>
				<Button
					content="Save"
					labelPosition="right"
					icon="checkmark"
					onClick={onSaveQuestion}
					positive
					disabled={isSaveDisabled()}
				/>
			</Modal.Actions>
		</Modal>
	);
};

export default QuestionFormPanel;
