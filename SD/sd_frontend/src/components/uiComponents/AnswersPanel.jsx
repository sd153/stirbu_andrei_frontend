import React, { useContext, useState } from "react";
import { Header, Comment, Form, Button, Icon, Label } from "semantic-ui-react";
import { UserContext } from "../../resources/UserContext";
import UserService from "../../services/UserService";
import SweetAlert from "react-bootstrap-sweetalert";

const AnswersPanel = ({
	answers,
	answerUsers,
	onAnswerSave,
	onAnswerDelete,
	onAnswerUpdate,
	onErrorLogin,
	onError,
	questionID,
}) => {
	const { user } = useContext(UserContext);
	const [answerText, setAnswerText] = useState("");
	const [editAnswer, setEditAnswer] = useState(null);
	const [msg, setMsg] = useState("");

	const findUserName = (answer) => {
		if (answerUsers.length === 0) {
			return "";
		}

		const foundUser = answerUsers.find((user) => user.id === answer.userID);
		if (foundUser === null) {
			return "";
		}

		return foundUser.name;
	};

	const findUserScore = (answer) => {
		if (answerUsers.length === 0) {
			return "";
		}

		const foundUser = answerUsers.find((user) => user.id === answer.userID);
		if (foundUser === null) {
			return "";
		}

		return foundUser.score;
	};

	const generateRandomAvatar = () => {
		const url = "https://react.semantic-ui.com/images/avatar/small";
		const names = ["matt", "joe", "jenny", "elliot"];
		const randomIndex = Math.floor(Math.random() * 3);
		return `${url}/${names[randomIndex]}.jpg`;
	};

	const onAnswerSaveClicked = () => {
		if (!user) {
			onErrorLogin("You must be logged in to add an answer.");
			return;
		}

		if (answerText.trim().length === 0) {
			return;
		}

		if (editAnswer) {
			onAnswerUpdate({ ...editAnswer, answerText: answerText });
			setAnswerText("");
			setEditAnswer(null);
			return;
		}

		const newAnswer = {
			answerText: answerText,
			answerDate: new Date(),
			answerVotes: 0,
			userID: user.id,
			questionID: questionID,
		};

		onAnswerSave(newAnswer);
		setAnswerText("");
	};

	const onAnswerEditClicked = (answer) => {
		if (!user) {
			onErrorLogin("You must be logged in to edit an answer.");
			return;
		}

		if (!user.isAdmin && user.id !== answer.userID) {
			onError("You must be the owner of the answer in order to edit it.");
			return;
		}

		setAnswerText(answer.answerText);
		setEditAnswer(answer);
	};

	const onAnswerDeleteClicked = (answer) => {
		if (!user) {
			onErrorLogin("You must be logged in to create a tag.");
			return;
		}

		if (!user.isAdmin && user.id !== answer.userID) {
			onError(
				"You must be the owner of the answer in order to delete it."
			);
			return;
		}

		onAnswerDelete(answer.id);
	};

	const onVoteAnswer = async (answer, value) => {
		if (!user) {
			onErrorLogin("You must be logged in to vote an answer.");
			return;
		}

		if (user.id === answer.userID) {
			onError("You can not vote your own answer");
			return;
		}

		if(value < 0) {
			await UserService.reduceUserScore(user.id);
		}
		
		onAnswerUpdate({ ...answer, answerVotes: answer.answerVotes + value });
	};

	const onBanUser = (userID) => {
		UserService.banUser(userID)
			.then(() => {
				setMsg("User banned successfully!");
			})
			.catch((error) => {
				onError("Something went wrong while banning a user");
			});
	};

	return (
		<Comment.Group
			style={{ margin: "0 auto", paddingTop: "5%" }}
			size="large"
		>
			<Header as="h3" dividing>
				Answers
			</Header>

			{answers.map((answer) => (
				<Comment>
					<Button.Group floated="right" size="mini">
						<Button
							size="mini"
							onClick={() => onVoteAnswer(answer, -1)}
						>
							<Icon name="minus" color="red" />
						</Button>
						<Button size="mini">{answer.answerVotes}</Button>
						<Button
							size="mini"
							onClick={() => onVoteAnswer(answer, 1)}
						>
							<Icon name="plus" color="green" />
						</Button>
					</Button.Group>
					<Comment.Avatar src={generateRandomAvatar()} />
					<Comment.Content>
						<Comment.Author as="a">
							{findUserName(answer)}
						</Comment.Author>
						<Comment.Metadata>
							<div>
								<Label color="teal" pointing="left" style={{marginRight: 5}}>
									{findUserScore(answer)}
								</Label>
								{answer.answerDate}
							</div>
						</Comment.Metadata>
						<Comment.Text>{answer.answerText}</Comment.Text>
						<Comment.Actions>
							<Comment.Action
								onClick={() => onAnswerEditClicked(answer)}
							>
								Edit
							</Comment.Action>
							<Comment.Action
								onClick={() => onAnswerDeleteClicked(answer)}
							>
								Delete
							</Comment.Action>
							{user &&
								user.isAdmin &&
								user.id !== answer.userID && (
									<Comment.Action
										onClick={() => onBanUser(answer.userID)}
									>
										Ban User
									</Comment.Action>
								)}
						</Comment.Actions>
					</Comment.Content>
				</Comment>
			))}

			<Form reply>
				<Form.TextArea
					value={answerText}
					onChange={(event, data) => setAnswerText(data.value)}
				/>
				<Button
					content={
						editAnswer ? "Edit Your Answer" : "Post Your Answer"
					}
					labelPosition="left"
					icon="edit"
					primary
					onClick={onAnswerSaveClicked}
				/>
			</Form>
			<SweetAlert
				info
				title={msg}
				onConfirm={() => {
					setMsg("");
				}}
				show={msg.length > 0}
				allowEscape={true}
				confirmBtnText="Ok"
				closeOnClickOutside={true}
			/>
		</Comment.Group>
	);
};

export default AnswersPanel;
