import React from "react";
import { Input, Icon } from "semantic-ui-react";

const TagFilter = ({ tags, onTagChange }) => {
	const onChange = (event, data) => {
		const value = data.value.toLowerCase();
		if (value.trim().length === 0) {
			onTagChange(tags);
			return;
		}
		const filteredTags = tags.filter((tag) =>
			tag.tagName.toLowerCase().includes(value)
		);
		onTagChange(filteredTags);
	};

	return (
		<Input icon placeholder="Search..." onChange={onChange}>
			<input />
			<Icon name="search" />
		</Input>
	);
};

export default TagFilter;
