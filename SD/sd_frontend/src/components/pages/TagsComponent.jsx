import React, { useEffect, useState, useContext } from "react";
import { UserContext } from "../../resources/UserContext";
import {
	Label,
	Dimmer,
	Loader,
	Grid,
	Header,
	Button,
	Input,
} from "semantic-ui-react";
import TagService from "../../services/TagService";
import SweetAlert from "react-bootstrap-sweetalert";
import TagFilter from "../uiComponents/TagFilter";
import { useHistory } from "react-router";

const TagsComponent = () => {
	const { user } = useContext(UserContext);
	const [tags, setTags] = useState([]);
	const [initialTags, setInitialTags] = useState([]);
	const [isLoading, setIsLoading] = useState(false);
	const [error, setError] = useState("");
	const [newTagName, setNewTagName] = useState("");
	const [errorLogin, setErrorLogin] = useState("");

	let history = useHistory();

	useEffect(() => {
		setIsLoading(true);

		TagService.getAll()
			.then((response) => {
				setTags(response.data);
				setInitialTags(response.data);
			})
			.catch((error) => {
				setError("Something went wrong while getting the tags");
			})
			.finally(() => {
				setIsLoading(false);
			});
	}, []);

	const onTagSave = () => {
		if (!user) {
			setErrorLogin("You must be logged in to create a tag.");
			return;
		}

		if (newTagName.trim().length === 0) {
			return;
		}

		setIsLoading(true);

		TagService.saveTag({ tagName: newTagName })
			.then((response) => {
				setTags((prevTags) => [...prevTags, response.data]);
				setInitialTags((prevTags) => [...prevTags, response.data]);
				setNewTagName("");
			})
			.catch((error) => {
				setError("Something went wrong while saving the tag");
			})
			.finally(() => {
				setIsLoading(false);
			});
	};

	const onErrorLoginClicked = () => {
		history.push("/login");
		setErrorLogin("");
	};

	return (
		<>
			<Grid>
				<Grid.Row>
					<Header as="h1" style={{ margin: "0 auto" }}>
						All Tags
					</Header>
				</Grid.Row>
			</Grid>
			<Grid style={{ margin: "0 auto", width: 1000, padding: 20 }} celled>
				{isLoading && (
					<Dimmer active>
						<Loader />
					</Dimmer>
				)}
				<Grid.Row>
					<TagFilter tags={initialTags} onTagChange={setTags} />
					<div style={{ marginLeft: "45.5%" }}>
						<Input
							placeholder="Add new Tag..."
							value={newTagName}
							onChange={(event, data) =>
								setNewTagName(data.value)
							}
						/>
						<Button
							content="Add a new Tag"
							color="teal"
							onClick={onTagSave}
						/>
					</div>
				</Grid.Row>
				<Grid.Row>
					{tags.map((tag) => (
						<Label as="a" tag style={{ margin: 20 }}>
							{tag.tagName}
						</Label>
					))}
				</Grid.Row>
				<SweetAlert
					type="danger"
					title={error}
					onConfirm={() => {
						setError("");
					}}
					onCancel={() => {
						setError("");
					}}
					cancelBtnText="Close"
					showCloseButton
					show={error.length > 0}
					allowEscape={true}
					confirmBtnText="Ok"
					closeOnClickOutside={true}
					timer={2000}
				/>
				<SweetAlert
					type="danger"
					title={errorLogin}
					onConfirm={onErrorLoginClicked}
					onCancel={() => {
						setErrorLogin("");
					}}
					cancelBtnText="Close"
					showCloseButton
					show={errorLogin.length > 0}
					allowEscape={true}
					confirmBtnText="Login?"
					closeOnClickOutside={true}
				/>
			</Grid>
		</>
	);
};

export default TagsComponent;
