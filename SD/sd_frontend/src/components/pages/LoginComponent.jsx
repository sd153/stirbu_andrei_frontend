import React, { useContext, useState } from "react";
import SweetAlert from "react-bootstrap-sweetalert";
import { Link, useHistory } from "react-router-dom";
import {
	Grid,
	Header,
	Form,
	Segment,
	Button,
	Message,
} from "semantic-ui-react";
import { UserContext } from "../../resources/UserContext";
import UserService from "../../services/UserService";

const LoginComponent = () => {
	const [loginInfo, setLoginInfo] = useState({
		username: "",
		password: "",
	});
	const [error, setError] = useState("");
	let history = useHistory();
	const { setUser } = useContext(UserContext);

	const handleChange = (field, value) => {
		setLoginInfo({ ...loginInfo, [field]: value });
	};

	const onLoginHandler = () => {
		if (
			loginInfo.username.trim().length === 0 ||
			loginInfo.password.trim().length === 0
		) {
			setError("Credentials should not be empty.");
			return;
		}
		UserService.login(loginInfo)
			.then((response) => {
				setUser(response.data);
				history.push("/home");
			})
			.catch(() => {
				setError("Invalid credentials.");
			});
	};

	return (
		<Grid
			textAlign="center"
			style={{ height: "80vh" }}
			verticalAlign="middle"
		>
			<Grid.Column style={{ maxWidth: 450 }}>
				<Header as="h2" color="teal" textAlign="center">
					Log-in to your account
				</Header>
				<Form size="large">
					<Segment stacked>
						<Form.Input
							fluid
							icon="user"
							iconPosition="left"
							placeholder="Username"
							onChange={(event) => {
								handleChange("username", event.target.value);
							}}
						/>
						<Form.Input
							fluid
							icon="lock"
							iconPosition="left"
							placeholder="Password"
							type="password"
							onChange={(event) => {
								handleChange("password", event.target.value);
							}}
						/>

						<Button
							color="teal"
							fluid
							size="large"
							onClick={onLoginHandler}
						>
							Login
						</Button>
					</Segment>
				</Form>
				<Message>
					New to us? <Link to="/register">Sign Up</Link>
				</Message>
				<SweetAlert
					type="danger"
					title={error}
					onConfirm={() => {
						setError("");
					}}
					onCancel={() => {
						setError("");
					}}
					show={error.length > 0}
					allowEscape={true}
					confirmBtnText="Ok"
					closeOnClickOutside={true}
				/>
			</Grid.Column>
		</Grid>
	);
};

export default LoginComponent;
