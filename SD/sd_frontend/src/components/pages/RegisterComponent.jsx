import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import {
	Grid,
	Header,
	Form,
	Segment,
	Button,
	Message,
	Icon,
	Label,
} from "semantic-ui-react";
import UserService from "../../services/UserService";
import SweetAlert from "react-bootstrap-sweetalert";
import { useHistory } from "react-router-dom";

const emptyModel = {
	username: "",
	name: "",
	password: "",
	confirmPassword: "",
	isAdmin: false,
	isBanned: false,
	score: 0,
};

const RegisterComponent = (props) => {
	const [registerInfo, setRegisterInfo] = useState(emptyModel);
	const [error, setError] = useState(emptyModel);
	const [isDisabled, setIsDisabled] = useState(true);
	const [isPasswordVisible, setIsPasswordVisible] = useState({
		normal: false,
		confirm: false,
	});
	const [isAlertOpen, setIsAlertOpen] = useState(false);
	const [requestError, setRequestError] = useState("");

	let history = useHistory();

	const handleChange = (field, value, type) => {
		if (type === "register") {
			setRegisterInfo({ ...registerInfo, [field]: value });
			validateInput(field, value);
		} else {
			setError({ ...error, [field]: value });
		}
	};

	const validateInput = (field, value) => {
		switch (field) {
			case "name":
				if (value.trim().length === 0) {
					handleChange("name", "Name should not be empty.", "error");
					return false;
				} else {
					handleChange("name", "", "error");
					break;
				}
			case "username":
				if (value.trim().length === 0) {
					handleChange(
						"username",
						"Username should not be empty.",
						"error"
					);
					return false;
				} else {
					handleChange("username", "", "error");
					break;
				}
			case "password":
				if (value.trim().length < 8) {
					handleChange(
						"password",
						"Password should have at least 8 characters.",
						"error"
					);
					return false;
				} else if (!/[0-9]/.test(value.trim())) {
					handleChange(
						"password",
						"Password should have at least 1 digit.",
						"error"
					);
					return false;
				} else if (!/[^A-Za-z0-9]/.test(value.trim())) {
					handleChange(
						"password",
						"Password should have at least 1 special character.",
						"error"
					);
					return false;
				} else {
					handleChange("password", "", "error");
					break;
				}
			case "confirmPassword":
				if (value.trim() !== registerInfo.password.trim()) {
					handleChange(
						"confirmPassword",
						"The passwords do not match.",
						"error"
					);
					return false;
				} else if (value.trim().length < 8) {
					handleChange(
						"confirmPassword",
						"Confirm password should have at least 8 characters.",
						"error"
					);
					return false;
				} else {
					handleChange("confirmPassword", "", "error");
					break;
				}
			default:
				break;
		}
		return true;
	};

	useEffect(() => {
		let isValid = true;

		Object.keys(error).forEach((e) => {
			if (error[e].length > 0) {
				isValid = false;
			}
		});

		setIsDisabled(!isValid);
	}, [error]);

	const validateAll = () => {
		let isValid = true;

		Object.keys(registerInfo).forEach((e) => {
			if (e !== "idClient") {
				isValid = isValid && validateInput(e, registerInfo[e]);
			}
		});

		return isValid;
	};

	const onSubmitHandler = () => {
		if (!validateAll()) {
			return;
		}

		const registerRequest = { ...registerInfo };
		delete registerRequest.confirmPassword;

		UserService.saveUser(registerRequest)
			.then(() => {
				setIsAlertOpen(true);
				setRegisterInfo(emptyModel);
			})
			.catch((err) => {
				setRequestError(
					"Something went wrong while registering the user"
				);
			})
			.finally(() => {
				setError(emptyModel);
			});
	};

	return (
		<Grid
			textAlign="center"
			style={{ height: "100vh" }}
			verticalAlign="middle"
		>
			<Grid.Column style={{ maxWidth: 450 }}>
				<Header as="h2" color="teal" textAlign="center">
					Sign up a new account
				</Header>
				<Form size="large">
					<Segment stacked>
						{error.name.length > 0 && (
							<Label basic color="red" pointing="below">
								{error.name}
							</Label>
						)}
						<Form.Input
							fluid
							icon="user outline"
							iconPosition="left"
							placeholder="Name"
							onChange={(event) => {
								handleChange(
									"name",
									event.target.value,
									"register"
								);
							}}
							className={error.name.length !== 0 ? "error" : ""}
						/>
						{error.username.length > 0 && (
							<Label basic color="red" pointing="below">
								{error.username}
							</Label>
						)}
						<Form.Input
							fluid
							icon="user"
							iconPosition="left"
							placeholder="Username"
							onChange={(event) => {
								handleChange(
									"username",
									event.target.value,
									"register"
								);
							}}
							className={
								error.username.length !== 0 ? "error" : ""
							}
						/>
						{error.password.length > 0 && (
							<Label basic color="red" pointing="below">
								{error.password}
							</Label>
						)}
						<Form.Input
							fluid
							icon="lock"
							iconPosition="left"
							placeholder="Password"
							type={
								!isPasswordVisible.normal ? "password" : "text"
							}
							action={{
								icon: !isPasswordVisible.normal
									? "eye"
									: "eye slash",
								onClick: () => {
									setIsPasswordVisible({
										...isPasswordVisible,
										normal: !isPasswordVisible.normal,
									});
								},
							}}
							onChange={(event) => {
								handleChange(
									"password",
									event.target.value,
									"register"
								);
							}}
							className={
								error.password.length !== 0 ? "error" : ""
							}
						/>
						{error.confirmPassword.length > 0 && (
							<Label basic color="red" pointing="below">
								{error.confirmPassword}
							</Label>
						)}
						<Form.Input
							fluid
							icon="lock"
							iconPosition="left"
							placeholder="Confirm Password"
							type={
								!isPasswordVisible.confirm ? "password" : "text"
							}
							action={{
								icon: !isPasswordVisible.confirm
									? "eye"
									: "eye slash",
								onClick: () => {
									setIsPasswordVisible({
										...isPasswordVisible,
										confirm: !isPasswordVisible.confirm,
									});
								},
							}}
							onChange={(event) => {
								handleChange(
									"confirmPassword",
									event.target.value,
									"register"
								);
							}}
							className={
								error.confirmPassword.length !== 0
									? "error"
									: ""
							}
						/>
						<Button
							color="teal"
							fluid
							size="large"
							onClick={onSubmitHandler}
							disabled={isDisabled}
						>
							Sign Up
						</Button>
					</Segment>
				</Form>
				<Message>
					<Icon name="help" />
					Already signed up?&nbsp;<Link to="/login">Login here</Link>
					&nbsp;instead.
				</Message>
				<SweetAlert
					type="success"
					title="User registered successfully!"
					onConfirm={() => {
						history.push("/");
					}}
					onCancel={() => setIsAlertOpen(false)}
					show={isAlertOpen}
					allowEscape={true}
					cancelBtnText="No"
					confirmBtnText="Yes"
					showCancel={true}
					closeOnClickOutside={true}
				>
					Go to Login Page?
				</SweetAlert>
				<SweetAlert
					type="danger"
					title={requestError}
					onConfirm={() => {
						setRequestError("");
					}}
					onCancel={() => {
						setRequestError("");
					}}
					show={requestError.length > 0}
					allowEscape={true}
					confirmBtnText="Ok"
					closeOnClickOutside={true}
				></SweetAlert>
			</Grid.Column>
		</Grid>
	);
};

export default RegisterComponent;
