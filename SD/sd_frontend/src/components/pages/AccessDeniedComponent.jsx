import React from "react";
import { useHistory } from "react-router";
import { Grid, Card, Icon, Button } from "semantic-ui-react";

const AccessDeniedComponent = () => {
	let history = useHistory();

	const redirectHandler = () => {
		history.push("/login");
	};

	return (
		<Grid centered verticalAlign="middle" className="access-denied">
			<Card>
				<Icon name="dont" size="huge" />
				<Card.Content>
					<Card.Header>Unauthorized Access</Card.Header>
					<Card.Description>
						Your account has been banned
					</Card.Description>
				</Card.Content>
				<Card.Content>
					<Button onClick={redirectHandler} color="teal">
						Go to Sign-in page
					</Button>
				</Card.Content>
			</Card>
		</Grid>
	);
};

export default AccessDeniedComponent;
