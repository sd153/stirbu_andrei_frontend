import React, { useContext, useState, useEffect } from "react";
import { useHistory } from "react-router";
import { Header, Button, Grid, Dimmer, Loader } from "semantic-ui-react";
import { UserContext } from "../../resources/UserContext";
import AnswerService from "../../services/AnswerService";
import QuestionService from "../../services/QuestionService";
import QuestionFormPanel from "../uiComponents/QuestionFormPanel";
import QuestionList from "../uiComponents/QuestionList";
import SweetAlert from "react-bootstrap-sweetalert";
import UserService from "../../services/UserService";
import QuestionFilter from "../uiComponents/QuestionFilter";
import TagService from "../../services/TagService";

const QuestionsComponent = () => {
	const { user } = useContext(UserContext);
	const [questions, setQuestions] = useState([]);
	const [isFormOpen, setIsFormOpen] = useState(false);
	const [errorQuestion, setErrorQuestion] = useState("");
	const [error, setError] = useState("");
	const [isLoading, setIsLoading] = useState(false);
	const [initialQuestions, setInitialQuestions] = useState([]);
	const [tags, setTags] = useState([]);

	let history = useHistory();

	const sortByDate = (list) => {
		return list.sort((a, b) => {
			return new Date(b.questionDate) - new Date(a.questionDate);
		});
	};

	// eslint-disable-next-line
	useEffect(async () => {
		try {
			setIsLoading(true);

			const [questionsR, tagsR] = await Promise.all([
				QuestionService.getAll(),
				TagService.getAll(),
			]);

			setTags(tagsR.data);
			let existingQuestions = questionsR.data;

			existingQuestions.forEach(async (question) => {
				const answerR = await AnswerService.countAnswersByQuestionId(
					question.id
				);
				const userR = await UserService.getUser(question.userID);

				question.userName = userR.data.name;
				question.answerCount = answerR.data;
			});

			setQuestions(sortByDate(existingQuestions));
			setInitialQuestions(sortByDate(existingQuestions));
		} catch (error) {
			setError(
				"Something went wrong while getting the data from the server"
			);
		} finally {
			setIsLoading(false);
		}
	}, []);

	const onFormClose = () => {
		setIsFormOpen(false);
	};

	const onAskQuestionClicked = () => {
		if (user) {
			setIsFormOpen(true);
		} else {
			setErrorQuestion("You must be logged in to ask a question.");
		}
	};

	const onErrorLoginClicked = () => {
		history.push("/login");
		setErrorQuestion("");
	};

	const onQuestionSave = async (question) => {
		try {
			setIsLoading(true);

			const savedQuestionR = await QuestionService.saveQuestion(question);
			const savedQuestion = savedQuestionR.data;

			const answerR = await AnswerService.countAnswersByQuestionId(
				savedQuestion.id
			);
			const userR = await UserService.getUser(savedQuestion.userID);

			savedQuestion.userName = userR.data.name;
			savedQuestion.answerCount = answerR.data;

			setQuestions((prevQuestions) => {
				return sortByDate([...prevQuestions, savedQuestion]);
			});
		} catch (error) {
			setError("Something went wrong while adding the question");
		} finally {
			setIsLoading(false);
		}
	};

	return (
		<Grid>
			{isLoading && (
				<Dimmer active>
					<Loader />
				</Dimmer>
			)}
			<Grid.Row>
				<Header as="h1" style={{ margin: "0 auto" }}>
					All Questions
				</Header>
			</Grid.Row>
			<Grid.Row style={{ marginLeft: "24.5%" }}>
				<QuestionFilter
					initialQuestions={initialQuestions}
					changeQuestions={setQuestions}
					tags={tags}
				/>
				<Button
					style={{ position: "absolute", right: "31.5%" }}
					content="Ask a question"
					color="teal"
					onClick={onAskQuestionClicked}
				/>
			</Grid.Row>
			<QuestionList questions={questions} />
			<QuestionFormPanel
				onClose={onFormClose}
				open={isFormOpen}
				onSave={onQuestionSave}
				onError={setError}
				userID={user ? user.id : 0}
				tags={tags}
			/>
			<SweetAlert
				type="danger"
				title={errorQuestion}
				onConfirm={onErrorLoginClicked}
				onCancel={() => {
					setErrorQuestion("");
				}}
				cancelBtnText="Close"
				showCloseButton
				show={errorQuestion.length > 0}
				allowEscape={true}
				confirmBtnText="Login?"
				closeOnClickOutside={true}
			/>
			<SweetAlert
				type="danger"
				title={error}
				onConfirm={() => {
					setError("");
				}}
				onCancel={() => {
					setError("");
				}}
				cancelBtnText="Close"
				showCloseButton
				show={error.length > 0}
				allowEscape={true}
				confirmBtnText="Ok"
				closeOnClickOutside={true}
				timer={2000}
			/>
		</Grid>
	);
};

export default QuestionsComponent;
