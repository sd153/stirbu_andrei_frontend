import React, { useState, useEffect, useContext } from "react";
import AnswersPanel from "../uiComponents/AnswersPanel";
import { useLocation } from "react-router-dom";
import {
	Button,
	Dimmer,
	Grid,
	Header,
	Icon,
	Label,
	Loader,
	TextArea,
	Form,
} from "semantic-ui-react";
import AnswerService from "../../services/AnswerService";
import QuestionService from "../../services/QuestionService";
import UserService from "../../services/UserService";
import SweetAlert from "react-bootstrap-sweetalert";
import { useHistory } from "react-router";
import { UserContext } from "../../resources/UserContext";

const sortByVotes = (list) => {
	return list.sort((a, b) => {
		return b.answerVotes - a.answerVotes;
	});
};

const QuestionComponent = () => {
	const { state } = useLocation();
	const [question, setQuestion] = useState(state.question);
	const [editQuestion, setEditQuestion] = useState(null);
	const [userQuestion, setUserQuestion] = useState();
	const [answerUsers, setAnswerUsers] = useState([]);
	const [answers, setAnswers] = useState([]);
	const [error, setError] = useState("");
	const [isLoading, setIsLoading] = useState(false);
	const [errorLogin, setErrorLogin] = useState("");
	const [confirmMsg, setConfirmMsg] = useState("");

	const { user } = useContext(UserContext);

	let history = useHistory();

	useEffect(() => {
		setIsLoading(true);
		Promise.all([
			AnswerService.getAllByQuestionId(question.id),
			UserService.getUser(question.userID),
		])
			.then(([rAnswers, rUser]) => {
				setAnswers(rAnswers.data);
				setUserQuestion(rUser.data);

				return Promise.all(
					rAnswers.data.map((answer) =>
						UserService.getUser(answer.userID)
					)
				);
			})
			.then((rUsers) => {
				setAnswerUsers(rUsers.map((response) => response.data));
			})
			.catch((error) => {
				setError("Something went wrong getting the data");
			})
			.finally(() => {
				setIsLoading(false);
			});
	}, [question.id, question.userID]);

	const onAnswerSave = (answer) => {
		setIsLoading(true);

		AnswerService.saveAnswer(answer)
			.then((response) => {
				setAnswers((prevAnswers) => [...prevAnswers, response.data]);
				return UserService.getUser(answer.userID);
			})
			.then((response) => {
				setAnswerUsers((prevAnswerUsers) => [
					...prevAnswerUsers,
					response.data,
				]);
			})
			.catch((error) => {
				setError("Something went wrong when saving the answer");
			})
			.finally(() => {
				setIsLoading(false);
			});
	};

	const onAnswerUpdate = (answer) => {
		setIsLoading(true);

		AnswerService.updateAnswer(answer)
			.then((response) => {
				setAnswers((prevAnswers) => {
					prevAnswers.find(
						(prevAnswer) => prevAnswer.id === answer.id
					).answerText = response.data.answerText;
					prevAnswers.find(
						(prevAnswer) => prevAnswer.id === answer.id
					).answerVotes = response.data.answerVotes;
					return prevAnswers;
				});
			})
			.catch((error) => {
				setError("Something went wrong when updating the answer");
			})
			.finally(() => {
				setIsLoading(false);
			});
	};

	const onAnswerDelete = (answerID) => {
		setIsLoading(true);

		AnswerService.deleteAnswer(answerID)
			.then((response) => {
				if (response === "Delete failed.") {
					setError("Something went wrong when deleting the answer.");
				}

				setAnswers((prevAnswers) =>
					prevAnswers.filter((answer) => answer.id !== answerID)
				);
			})
			.catch((error) => {
				setError("Something went wrong when deleting the answer.");
			})
			.finally(() => {
				setIsLoading(false);
			});
	};

	const onErrorLoginClicked = () => {
		history.push("/login");
		setErrorLogin("");
	};

	const onQuestionUpdate = (questionToUpdate, isVoting = false) => {
		if (!user) {
			setErrorLogin("You must be logged in to vote a question");
			return;
		}

		if (user.id === question.userID && isVoting) {
			setError("You can not vote your own question");
			return;
		}

		setIsLoading(true);

		QuestionService.updateQuestion(questionToUpdate)
			.then((response) => {
				setQuestion(response.data);
			})
			.catch((error) => {
				setError("Something went wrong updating the question");
			})
			.finally(() => {
				setIsLoading(false);
			});
	};

	const onEditClicked = () => {
		if (!user) {
			setErrorLogin("You must be logged in to edit a question");
			return;
		}

		if (!user.isAdmin && user.id !== question.userID) {
			setError("You can not edit somebody's else question");
			return;
		}

		if (!editQuestion) {
			setEditQuestion(question);
			return;
		}

		onQuestionUpdate(editQuestion);
		setEditQuestion(null);
	};

	const onQuestionTextChange = (event, data) => {
		setEditQuestion((prevQuestion) => ({
			...prevQuestion,
			questionText: data.value,
		}));
	};

	const onDeleteClicked = () => {
		if (!user) {
			setErrorLogin("You must be logged in to delete a question");
			return;
		}

		if (!user.isAdmin && user.id !== question.userID) {
			setError("You can not delete somebody's else question");
			return;
		}

		setConfirmMsg("Are you sure you want to delete the question?");
	};

	const onDelete = async () => {
		setIsLoading(true);

		if(answers.length > 0) {
			const deleteAnswers = answers.map((answer) => AnswerService.deleteAnswer(answer.id));

			try{
				await Promise.all(deleteAnswers);
			}catch{
				setError("Something went wrong while deleting the answers");
			}
		}

		QuestionService.deleteQuestion(question.id)
			.then((response) => {
				if (response.data === "Delete failed.") {
					setError(
						"Something went wrong while deleting the question"
					);
				}

				history.push("/");
			})
			.catch((error) => {
				setError("Something went wrong while deleting the question");
			})
			.finally(() => {
				setConfirmMsg("");
				setIsLoading(false);
			});
	};

	return (
		<Grid centered columns={2}>
			{isLoading && (
				<Dimmer active>
					<Loader />
				</Dimmer>
			)}
			<Grid.Column>
				<Header as="h1" textAlign="center">
					{question.title}
				</Header>
				<Header textAlign="center" as="h5">
					Created on: <strong>{question.questionDate}</strong>
				</Header>

				<Form>
					<TextArea
						value={
							editQuestion
								? editQuestion.questionText
								: question.questionText
						}
						readOnly={editQuestion === null}
						style={{ minHeight: 200, marginBottom: 20 }}
						onChange={onQuestionTextChange}
					/>
					<Button.Group floated="right" size="small">
						<Button
							color={editQuestion ? "teal" : "gray"}
							onClick={onEditClicked}
						>
							Edit
						</Button>
						<Button color="red" onClick={onDeleteClicked}>
							Delete
						</Button>
					</Button.Group>
				</Form>

				<Grid.Row style={{ marginBottom: 20 }}>
					{question.tagList.map((tag) => (
						<Label as="a" key={tag.id}>
							{tag.tagName}
						</Label>
					))}
				</Grid.Row>

				<Label style={{ padding: 12 }}>
					<Icon name="user" /> Author:
					<Label.Detail>
						{userQuestion === undefined
							? ""
							: userQuestion.username}
					</Label.Detail>
				</Label>

				<Button.Group floated="right">
					<Button
						onClick={() =>
							onQuestionUpdate(
								{
									...question,
									questionVotes: question.questionVotes - 1,
								},
								true
							)
						}
					>
						<Icon name="minus" color="red" />
					</Button>
					<Button>{question.questionVotes}</Button>
					<Button
						onClick={() =>
							onQuestionUpdate(
								{
									...question,
									questionVotes: question.questionVotes + 1,
								},
								true
							)
						}
					>
						<Icon name="plus" color="green" />
					</Button>
				</Button.Group>

				<AnswersPanel
					answers={sortByVotes(answers)}
					answerUsers={answerUsers}
					onAnswerSave={onAnswerSave}
					onErrorLogin={setErrorLogin}
					questionID={question.id}
					onError={setError}
					onAnswerDelete={onAnswerDelete}
					onAnswerUpdate={onAnswerUpdate}
				/>
			</Grid.Column>
			<SweetAlert
				type="danger"
				title={error}
				onConfirm={() => {
					setError("");
				}}
				onCancel={() => {
					setError("");
				}}
				cancelBtnText="Close"
				showCloseButton
				show={error.length > 0}
				allowEscape={true}
				confirmBtnText="Ok"
				closeOnClickOutside={true}
				timer={2000}
			/>
			<SweetAlert
				type="danger"
				title={errorLogin}
				onConfirm={onErrorLoginClicked}
				onCancel={() => {
					setErrorLogin("");
				}}
				cancelBtnText="Close"
				showCloseButton
				show={errorLogin.length > 0}
				allowEscape={true}
				confirmBtnText="Login?"
				closeOnClickOutside={true}
			/>
			<SweetAlert
				info
				title={confirmMsg}
				onConfirm={onDelete}
				onCancel={() => {
					setConfirmMsg("");
				}}
				cancelBtnText="Deny"
				showCancel
				show={confirmMsg.length > 0}
				allowEscape={true}
				confirmBtnText="Confirm"
				closeOnClickOutside={true}
			/>
		</Grid>
	);
};

export default QuestionComponent;
