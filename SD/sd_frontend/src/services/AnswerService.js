import axios from 'axios'
import { ANSWER_URL } from './UrlMapping';

class AnswerService {
    getAll() {
        return axios.get(`${ANSWER_URL}/getAll`);
    }

    getAllByQuestionId(questionId) {
        return axios.get(`${ANSWER_URL}/getAll/${questionId}`);
    }

    getAnswer(id) {
        return axios.get(`${ANSWER_URL}/getAnswer/${id}`);
    }

    deleteAnswer(id){
        return axios.delete(`${ANSWER_URL}/deleteAnswer/${id}`);
    }

    saveAnswer(answer) {
        return axios.post(`${ANSWER_URL}/saveAnswer`, answer);
    }

    updateAnswer(answer) {
        return axios.put(`${ANSWER_URL}/updateAnswer`, answer);
    }

    countAnswersByQuestionId(questionId) {
        return axios.get(`${ANSWER_URL}/count/${questionId}`);
    }
}

export default new AnswerService();