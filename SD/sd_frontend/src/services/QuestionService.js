import axios from 'axios'
import { QUESTION_URL } from './UrlMapping';

class QuestionService {
    getAll() {
        return axios.get(`${QUESTION_URL}/getAll`)
    }

    getQuestion(id) {
        return axios.get(`${QUESTION_URL}/getQuestion/${id}`);
    }

    deleteQuestion(id){
        return axios.delete(`${QUESTION_URL}/deleteQuestion/${id}`);
    }

    saveQuestion(question) {
        return axios.post(`${QUESTION_URL}/saveQuestion`, question);
    }

    updateQuestion(question) {
        return axios.put(`${QUESTION_URL}/updateQuestion`, question);
    }
}

export default new QuestionService();