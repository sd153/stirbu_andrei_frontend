import axios from 'axios'
import { TAG_URL } from './UrlMapping';

class TagService {
    getAll() {
        return axios.get(`${TAG_URL}/getAll`)
    }

    getTag(id) {
        return axios.get(`${TAG_URL}/getTag/${id}`);
    }

    deleteTag(id){
        return axios.delete(`${TAG_URL}/deleteTag/${id}`);
    }

    saveTag(tag) {
        return axios.post(`${TAG_URL}/saveTag`, tag);
    }

    updateTag(tag) {
        return axios.put(`${TAG_URL}/updateTag`, tag);
    }
}

export default new TagService();