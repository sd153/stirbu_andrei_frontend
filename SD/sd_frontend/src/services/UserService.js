import axios from 'axios'
import { USER_URL } from './UrlMapping';

class UserService {
    login(loginDTO) {
        return axios.post(`${USER_URL}/login`, loginDTO);
    }

    getAll() {
        return axios.get(`${USER_URL}/getAll`)
    }

    getUser(id) {
        return axios.get(`${USER_URL}/getUser/${id}`);
    }

    deleteUser(id){
        return axios.delete(`${USER_URL}/deleteUser/${id}`);
    }

    saveUser(user) {
        return axios.post(`${USER_URL}/saveUser`, user);
    }

    updateUser(user) {
        return axios.put(`${USER_URL}/updateUser`, user);
    }

    banUser(id) {
        return axios.put(`${USER_URL}/banUser/${id}`);
    }

    reduceUserScore(id) {
        return axios.put(`${USER_URL}/reduceScore/${id}`);
    }
}

export default new UserService();