import "./App.css";
import React, { useState } from "react";
import {
	BrowserRouter as Router,
	Route,
	Switch,
	Redirect,
} from "react-router-dom";
import { UserContext } from "./resources/UserContext";
import NavigationComponent from "./components/uiComponents/NavigationComponent";
import QuestionsComponent from "./components/pages/QuestionsComponent";
import QuestionComponent from "./components/pages/QuestionComponent";
import AccessDeniedComponent from "./components/pages/AccessDeniedComponent";
import LoginComponent from "./components/pages/LoginComponent";
import RegisterComponent from "./components/pages/RegisterComponent";
import TagsComponent from "./components/pages/TagsComponent";

const App = () => {
	const [user, setUser] = useState(null);

	return (
		<UserContext.Provider value={{ user, setUser }}>
			<Router>
				<NavigationComponent />
				<Switch>
					<Route
						exact
						path="/question/:questionId"
						component={QuestionComponent}
					/>
					<Route exact path="/" render={() =>
							user && user.isBanned ? (
								<Redirect to="/access-denied" />
							) : (
								<QuestionsComponent />
							)
						} />
					<Route
						exact
						path="/tags"
						render={() =>
							user && user.isBanned ? (
								<Redirect to="/access-denied" />
							) : (
								<TagsComponent />
							)
						}
					/>
					<Route path="/login" exact component={LoginComponent} />
					<Route
						exact
						path="/register"
						component={RegisterComponent}
					/>
					<Route
						path="/access-denied"
						exact
						component={AccessDeniedComponent}
					/>
					<Route
						render={() =>
							user && user.isBanned ? (
								<Redirect to="/access-denied" />
							) : (
								<Redirect to="/" />
							)
						}
					/>
				</Switch>
			</Router>
		</UserContext.Provider>
	);
};

export default App;
